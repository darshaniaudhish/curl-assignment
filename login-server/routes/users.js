const express = require('express');
const User = require('../models/users');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const passport = require('passport');
const router = express.Router();


/* POST register client */
router.post('/register', function(req, res, next) {

  let newUser = new User({
    fullName : req.body.fullName,
    companyName: req.body.company,
    username: req.body.username,
    password: req.body.password,
    role: req.body.role,
  });
  User.addUser(newUser, (err,user)=>{
    if(err){
      console.log(err.message);
      res.json({success:false, msg: "Failed to register user"});
    }
    res.json({success:true,msg:"User registered"});
  });
});

/* POST authenticate client */
router.post('/authenticate', function(req, res, next) {

  let username = req.body.username;
  let password = req.body.password;

  User.getUserByUsername(username, (err,user)=>{
    if(err){
      throw err;
    }
    if(!user){
      rs.json({success:false, msg:"User not found"});
    }else{
      User.comparePassword(password,user.password,(err,isMatch)=>{
        if(err){
          throw err;
        }
        if(isMatch){
          const token = jwt.sign({user}, config.secret, {
            expiresIn : '24h',
          });
          res.json({success:true,
            token : 'JWT '+token,
            user: {
              id: user.username,
              fullName: user.fullName,
              company: user.company,
            }
          })
        }else{
          res.json({success: false, msg: 'Wrong password'});
        }
      });
    }
  });

});


router.get('/profile',
  passport.authenticate('jwt',{session:false}),
  (req,res,next) =>{
    res.send('SUCCESS!!!');
  }
  );


module.exports = router;
