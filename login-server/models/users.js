const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const config =  require('../config/database');

const UserSchema = mongoose.Schema({
    fullName:{
        type: String,
        maxlength: 32,
        trim: true,
    },
    companyName: {
        type: String,

    },
    username:{
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    password:{
        type:String,
        required: true,
        maxlength: 128,
    },
    salt:{
        type: String,
    },
    role:{
        type: String,
        required: true,
        default: 0, //0 is client access
    }
});

const User = module.exports = mongoose.model('User',UserSchema);

module.exports.addUser = function(newUser, callback){
    bcryptjs.genSalt(10,(err,salt)=>{
        bcryptjs.hash(newUser.password, salt, (error,hash)=>{
            if(error){
                throw error;
            }
            newUser.salt = salt;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.getUserByUsername = function(username,callback){
    const query = {username:username};
    User.findOne(query,callback);
}

module.exports.getUserById = function(id, callback){
    User.findById(id,callback);
}

module.exports.comparePassword = function(userPassword, hashedPassword, callback){
    bcryptjs.compare(userPassword,hashedPassword, (err, isMatch)=>{
        if(err) throw err;
        callback(null,isMatch);
    });
}

module.exports.authenticate = async function(plainText, salt, hashedPassword){

    let isAuthorized = false;
    await  bcryptjs.hash(plaintText,salt, (error,hash) =>{
        if(error){
            throw error;
        }
        if(hash == hashedPassword){
            isAuthorized = true;
        }
    });
    return isAuthorized;
}
